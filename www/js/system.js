document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
    document.addEventListener("backbutton", onBackKeyDown, false);
}

var try_to_back = 1;

function onBackKeyDown() {
    console.log("activity_menu : " + activity_menu);
    console.log("activity : " + activity);
    console.log("activity_zoom : " + activity_zoom);
    if (activity_menu == "open-menu") {
        $(".side-bar-background-col").click();
        activity_menu = "";
    } else if (activity_zoom == 2) {
        console.log("activity_zoom : " + activity_zoom);
        activity_zoom = 1;
        console.log("activity_zoom : " + activity_zoom);
        $(".close").click();
    } else {
        if (activity == "open kategori modal") {
            hide_kategori();
        } else if (activity == 'mobil detail view'
            || activity == 'mobil list'
            || activity == 'news list'
            || activity == 'news detail'
            || activity == 'about us'
        ) {
            try_to_back++;
            console.log("try to back : " + try_to_back);
            if (try_to_back > 1) {
                window.location = 'index.html';
            } else {
                window.history.back();
            }
        } else {
            merek = getUrlParameter("merek");
            nama = getUrlParameter("nama");
            promo = getUrlParameter("promo");
            if (merek || nama || promo) {
                window.location = 'index.html';
            } else {
                exitModal();
            }
        }
    }
}

function generateMerekMenu()
{
    $.get(BASE_URL + "merek", function(data){
        obj = JSON && JSON.parse(data) || $.parseJSON(data);
        html = '<button class="btn btn-default kategori-btn" kategori-data="all">Lihat Semua Merek</button>';
        $.each(obj, function(key, val) {
            html = html + '<button class="btn btn-default kategori-btn" kategori-data="' + val.merek + '">' + val.label + '</button>';
        });

        html = html + '<script>';
        html = html + '$(".kategori-btn").click(function(){';
        html = html + 'kategori = $(this).attr("kategori-data");';
        html = html + 'console.log("kategori : " + kategori);';
        html = html + 'if (kategori == "all") {';
        html = html + '    window.location = "index.html";';
        html = html + '} else {';
        html = html + '    window.location = "index.html?merek=" + kategori;';
        html = html + '}';
        html = html + ' });';
        html = html + '</script>';
        $(".merek-list").html(html);
    });
}

generateMerekMenu();

function exitModal() {
    title = $(".modal-title").html();
    merek_list = $(".merek-list").html();
    html = "";

    $(".modal-title").html("<h4></h4>");

    html = html + '<div id="exit-confirm" style="background-color:#fff;padding:20px;text-align:center;">'
    + '<h4>Apakah anda akan keluar dari aplikasi ini?</h4>'
    + '<button class="btn btn-danger" onclick="navigator.app.exitApp(); console.log(\'aplication exit\');">Ya</button>&nbsp'
    + '<button class="btn btn-default" onclick="$(\'.dimiss-modal-kategori\').click();">Tidak</button><br>'
    + '</div>'
    + '<script>$(".kategori-btn").hide(); $(".dimiss-modal-kategori").hide();<\/script>'
    ;

    $(".merek-list").html(html);
    $(".kategori-button").click();
}

function closeApplication()
{
    console.log("Exit trigger");
    navigator.app.exitApp();
}

$(document).on('hide.bs.modal','#modal-merek', function () {
    generateMerekMenu();
    $(".modal-title").html("<h4>Pilih Merek Kendaraan</h4>");
});
