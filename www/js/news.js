$(document).ready(function(){

    function getNews(page) {

        param = "";
        param = "?page=" + page;

        $.get(BASE_URL + "news" + param, function(data){
            obj = JSON && JSON.parse(data) || $.parseJSON(data);
            html = "";
            $.each(obj, function(key, val) {
                now = new Date();
                tanggal = dateFormat(val.date_update, "d mmmm yyyy");
                html = html + '<div class="news-row" data-id-news="'+val.id+'">';
                html = html + '<div class="row banner-item">';
                html = html + '<div class="col-xs-4 item-image">';
                html = html + '<img class="pic">';
                html = html + '</div><div id="hidden-content" style="display:none">'+val.content+'</div>';
                html = html + '<div class="col-xs-8 item-description">';
                html = html + '<p class="item-title">'+val.judul+'</p>';
                html = html + '<small>'+tanggal+'</small>';
                html = html + '</div></div></div>';
            });

            html = html + '<div id="new-news"></div>';
            html = html + '<script>';
            html = html + '$(".news-row").click(function(){';
            html = html + 'id_news = $(this).attr("data-id-news");';
            html = html + 'window.location = "new.html?id=" + id_news;';
            html = html + '});';
            html = html + 'img = $("#hidden-content img").attr("src");';
            html = html + 'if (img) {';
            html = html + '$(".pic").attr("src", img);';
            html = html + '} else {';
            html = html + 'IMAGE_BASE_URL = BASE_URL.replace("api/", "");';
            html = html + 'img = IMAGE_BASE_URL + "asset/img/no_image.png";';
            html = html + '$(".pic").attr("src", img);';
            html = html + '}';
            html = html + '<\/script>';

            $("#new-news").replaceWith(html);
        });
    }

    getNews(1);

    var $container = $(".main-container");
    var i = 1;
    $(window).lazyScrollLoading({
        onScrollToBottom : function(e, $lazyItems) {
            //$container.append($(html));
            i++;
            getNews(i);
        }
    });
});
