$(document).ready(function(){
    // $("body").on("swiperight",function(){
    //     $(".menu-sidebar-btn").click();
    // });
    // $("body").on("swipeleft",function(){
    //     $(".side-bar-background-col").click();
    // });

    $(".home-btn").click(function(){
        window.location = "index.html";
    });

    $(".side-bar-background-col").click(function(){
        $(".side-bar-background").hide();
        $(".side-bar").animate({left:-250, opacity:"hide"}, 900);
    });

    $(".close-side-click").click(function(){
        $(".side-bar-background-col").click();
    });

    $(".menu-sidebar-btn").click(function(){
        $(".side-bar-background").show();
        $(".side-bar").animate({left:250, opacity:"show"}, 900);
        activity_menu = "open-menu";
    });

    $(".menu-search-btn").click(function(){
        $(".search-box").fadeIn();
    });

    $(".menu-btn").click(function(){
        target = $(this).attr("target-data");
        $(".side-bar-background").hide();
        $(".side-bar").hide();
        if (target == 'jilid') {
            window.location = "index.html";
        } else if (target == "bookmark") {
            window.location = 'bookmark.html';
        } else if (target == "tentang-kibar") {
            window.location = 'tentang-kibar.html';
        } else if (target == "materi") {
            window.location = 'materi.html';
        } else if (target == "developer") {
            window.location = 'developer.html';
        }
    });

    $(".curent-user").click(function(){
        window.location = 'profil.html';
    });

    $(".search-go").click(function(){
        search = $(".search-param").val();
        window.location = "index.html?nama=" + search;
    });

    $(".search-param").keyup(function(e){
        var code = e.which; // recommended to use e.which, it's normalized across browsers
        if(code==13)e.preventDefault();
        if(code==32||code==13||code==188||code==186){
            $(".search-go").click();
        } // missing closing if brace
    });

    $(".stok-mobil-btn").click(function(){
        show_kategori();
    });

    $(".mobil-row").click(function(){
        id_mobil = $(this).attr("data-id-mobil");
        console.log("mobil row : " + id_mobil);
        window.location = "mobil.html?id=" + id_mobil;
    });

    $(".news-row").click(function(){
        id_news = $(this).attr("data-id_news");
        window.location = "new.html?id=" + id_news;
    });

    $(".android-version").html(ANDROID_VERSION);
});
