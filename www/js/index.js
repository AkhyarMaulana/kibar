var page = 0;
var get_mobil = "";

query_merek = getUrlParameter("merek");
console.log("GET query merek : " + query_merek);
if (query_merek) {
    var merek = query_merek;
} else {
    var merek = "";
}

query_promo = getUrlParameter("promo");
console.log("GET query promo : " + query_promo);
if (query_promo) {
    var promo = query_promo;
} else {
    var promo = "";
}

query_nama = getUrlParameter("nama");
console.log("GET query nama : " + query_nama);
if (query_nama) {
    var nama = query_nama;
    $(".search-param").val(nama);
    $(".search-box").show();
} else {
    var nama = "";
}

function getMobil(page)
{
    param = "";
    param = "?page=" + page;

    if (merek != "") {
        param = param + "&merek=" + merek;
    }

    if (nama != "") {
        param = param + "&nama=" + nama;
    }

    if (promo != "") {
        param = param + "&promo=" + promo;
    }

    console.log("loaded page : " + page);

    $.get(BASE_URL + "mobil" + param, function(data){
        if (data == "[]" && page == 1) {
            $(".new-row-mobil").replaceWith("<h2 style='text-align:center'>Mobil belum tersedia</h2>");
        } else if (data == "[]" && page > 1) {
            $(".new-row-mobil").replaceWith("");
            $(".loading-animated").hide();
        }
        i = 0;
        obj = JSON && JSON.parse(data) || $.parseJSON(data);
        $.each(obj, function(key, val) {
            i++;
            row = '';
            if (i == 1) {
                special_id = "id='mobil-row'";
                special_main_mobil = "id='main-mobil'";
            } else {
                special_id = "";
                special_main_mobil = "";
            }
            row = row + '<div class="col-xs-12 col-sm-6 mobil-row" ' + special_id + ' data-id-mobil="'+val.id+'">';
            row = row + '<div class="row banner-item">';
            row = row + '<div class="col-xs-5 item-image">';
            if (val.soldout == "yes") {
                row = row + '<img src="image/sold-out.png" class="promo-label">';
            } else if (val.terdp == "yes") {
                row = row + '<img src="image/ter-dp.png" class="promo-label">';
            } else if (val.jenis_potongan != "no" || val.giveaway != '') {
                row = row + '<img src="image/promo.png" class="promo-label">';
            }
            row = row + '<div class="main-img" '+special_main_mobil+' style="background-image: url(\''+val.photo[0]+'\')"></div>';
            row = row + '</div>';
            row = row + '<div class="col-xs-7 item-description">';
            row = row + '<p class="item-title">'+val.nama+'</p>';
            is_promo = "no";
            if (val.potongan_rupiah > 0) {
                harga_publik = parseInt(val.harga) - parseInt(val.potongan_rupiah);
                harga_disable = val.harga;
                is_promo = "yes"
            } else if (val.potongan_diskon > 0) {
                harga_publik = parseInt(val.harga) * (100 - val.potongan_diskon) / 100;
                harga_disable = val.harga;
                is_promo = "yes"
            } else {
                harga_publik = val.harga;
                harga_disable = "";
            }
            row = row + '<p class="curent-price">'+toRupiah(harga_publik)+'</p>';
            if (is_promo == "yes") {
                row = row + '<del><small class="disable-price">'+toRupiah(harga_disable)+'</small></del>';
            }

            if (val.giveaway != "") {
                row = row + '<small class="disable-price">'+val.giveaway+'</small>';
            }

            if (val.jenis_potongan == "dp minimal" && val.dp_minimal) {
                row = row + '<div class="dp-minimal-container" style="max-width:80%">';
                row = row + '<p><b>DP Minimal</b></p>';
                row = row + '<p>'+ toRupiah(val.dp_minimal) +'</p>';
                row = row + '</div>';
            }

            row = row + '<p class="alamat-holder"><i class="glyphicon glyphicon-map-marker red-map"></i>'+val.cabang.nama_cabang+'</p>';
            row = row + '</div></div></div>';
            row = row + '<div class="new-row-mobil">';
            row = row + '<script>';
            row = row + '$(".mobil-row").click(function(){';
            row = row + 'id_mobil = $(this).attr("data-id-mobil");';
            row = row + 'console.log("mobil row : " + id_mobil);';
            row = row + 'window.location = "mobil.html?id=" + id_mobil;';
            row = row + '});';
            //row = row + '$(document).resize(function(){';
            row = row + 'if ($(document).width() < 400) { ';
            row = row + '$(".main-img").css("background-position", "top center");';
            row = row + '$(".main-img").css("height", "170px");';
            row = row + '$(".main-img").css("background-size", "120% auto");';
            row = row + '} else {';
            row = row + '$(".main-img").css("background-position", "center");';
            row = row + '$(".main-img").css("height", "170px");';
            row = row + '$(".main-img").css("background-size", "120% auto");';
            row = row + '}';
            //row = row + '});';
            row = row + '<\/script>';
            row = row + '</div>';
            $(".new-row-mobil").replaceWith(row);
        });
    });
}

page = page + 1;
getMobil(page);

$(document).ready(function() {
    var $container = $(".main-container");
    var i = 1;
    $(window).lazyScrollLoading({
        onScrollToBottom : function(e, $lazyItems) {
            //$container.append($(html));
            i++;
            getMobil(i);
        }
    });
});