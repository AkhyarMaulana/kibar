$(document).ready(function(){
    query_id = getUrlParameter("id");
    $.get(BASE_URL + "single/news?id=" + query_id, function(data){
        obj = JSON && JSON.parse(data) || $.parseJSON(data);
        tanggal = dateFormat(obj.date_update, "d mmmm yyyy");
        $("#content-news").html(obj.content);
        $("#judul-news").html(obj.judul);
        $("#tanggal-post").html(tanggal);
    });
});
