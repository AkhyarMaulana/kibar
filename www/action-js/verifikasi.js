$("#to-daftar-page-action").click(function(){
    window.location = "register.html";
});

$("#to-login-page-action").click(function(){
    window.location = "login.html";
});

$("#verivication-action").click(function(){
    restPost(
        BASE_URL + 'api/verifikasi',
        {},
        {
            email               : $("#email").val(),
            password            : $("#password").val(),
            verification_code    : $("#verification_code").val(),
        },
        function (status, data) {
            obj = data.responseJSON;

            if (status == 'success') {
                console.log(obj);
                html_success = "<p>" + obj.message + "</p>";
                $(".success-alert").html(html_success);
                $(".error-alert").hide();
                $(".success-alert").show();
            } else {
                console.log(obj);
                html_error = "";
                error_message = obj.data;
                if (error_message) {
                    $.each(error_message, function(key, val){
                        html_error = html_error + "<p>" + val + "</p>";
                    });
                } else {
                    html_error = obj.message;
                }

                $(".error-alert").html(html_error);
                $(".error-alert").show();
                $(".success-alert").hide();
            }
        }
    );
});
