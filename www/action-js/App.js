function restPost(url, header, data, result)
{
    /*
    javascript header http
    https://stackoverflow.com/questions/7100294/json-post-with-customized-httpheader-field
    */
    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(data),
        headers: header,
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        dataType: "json",
        success: function (data, status, jqXHR) {
            result(status ,jqXHR);
        },
        error: function (jqXHR, status) {
            data = {};
            result(status, jqXHR);
        }
    });
}

function restGET(url, header, result)
{
    $.ajax({
        type: "GET",
        url: url,
        headers: header,
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        dataType: "json",
        success: function (data, status, jqXHR) {
            result(status ,jqXHR);
        },
        error: function (jqXHR, status) {
            data = {};
            result(status, jqXHR);
        }
    });
}

function validation(type, alias, data)
{
    return null;
}
