$("#to-login").click(function(){
    window.location = 'login.html';
});

$("#to-register").click(function(){
    window.location = 'register.html';
});

$("#reset-password").click(function(){
    restPost(BASE_URL + "api/resetpassword",{
    }, {
        kode : $("#kode").val(),
        password : $("#password").val(),
        password_confirm : $("#password-confirm").val(),
    },function(status, data){
        obj = data.responseJSON;
        console.log(obj);
        if (status == 'success') {
            $(".success-alert").html("<p>Password berhasil diubah silahkan login untuk melanjutkan</p>");
            $(".success-alert").show();
            $(".error-alert").hide();
            $(".info-alert").hide();
        } else {
            var error_message = "";
            if (obj.data) {
                $.each(obj.data, function(key, val){
                    error_message = error_message + "<p>" + val + "</p>";
                });
            } else {
                error_message = "<p>" + obj.message + "</p>";
            }
            $(".error-alert").html(error_message);
            $(".error-alert").show();
        }
    });
});
