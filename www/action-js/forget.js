$("#to-login").click(function(){
    window.location = 'login.html';
});

$("#to-register").click(function(){
    window.location = 'register.html';
});


$("#to-reset").click(function(){
    window.location = 'reset-password.html';
});

$("#forget-action").click(function(){
    restPost(BASE_URL + "api/forget",{
    }, {
        email : $("#email").val(),
    },function(status, data){
        obj = data.responseJSON;
        console.log(obj);
        if (status == 'success') {
            window.location = "reset-password.html";
        } else {
            var error_message = "";
            if (obj.data) {
                $.each(obj.data, function(key, val){
                    error_message = error_message + "<p>" + val + "</p>";
                });
            } else {
                error_message = "<p>" + obj.message + "</p>";
            }
            $(".error-alert").html(error_message);
            $(".error-alert").show();
        }
    });
});
