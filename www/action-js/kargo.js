$("#cek-resi").click(function(){
    $(".loading-animated").show();
    $("#error-alert").hide();
    restGET(BASE_URL + "api/kargo?kode_resi=" + $("#resi").val(),{
        "Acc-Id"  : localStorage.getItem('ACC_ID'),
        "Acc-Key" : localStorage.getItem('ACC_KEY')
    }, function(status, data){
        if (status == 'success') {
            obj = data.responseJSON;
            console.log(obj);
            $("#error-alert").hide();
            var html = "";
            html = html + "<tr><td colspan='2'><b>Detail Pengiriman</b></td>";
            html = html + "<tr><td>Tanggal Masuk</td>";
            html = html + "<td>"+tanggal_indo(obj.data.tanggal)+"</td></tr>";
            html = html + "<tr><td>Kota Asal</td>";
            html = html + "<td>"+(obj.data.kota_asal)+"</td></tr>";
            html = html + "<tr><td>Kota Tujuan</td>";
            html = html + "<td>"+(obj.data.tujuan)+"</td></tr>";
            html = html + "<tr><td>Kantro TO</td>";
            html = html + "<td>"+(obj.data.kantor_to)+"</td></tr>";
            html = html + "<tr><td>Atas Nama</td>";
            html = html + "<td>"+(obj.data.nama)+"</td></tr>";
            html = html + "<tr><td>Telepon</td>";
            html = html + "<td>"+(obj.data.telpon)+"</td></tr>";
            html = html + "<tr><td colspan='2'><b>Detail Paket</b></td>";
            html = html + "<tr><td>Deskripsi paket</td>";
            html = html + "<td>"+(obj.data.keterangan)+"</td></tr>";
            html = html + "<tr><td>Jumlah</td>";
            html = html + "<td>"+(obj.data.qty)+"</td></tr>";
            html = html + "<tr><td>Berat</td>";
            html = html + "<td>"+(obj.data.berat)+"</td></tr>";
            html = html + "<tr><td>Harga Satuan</td>";
            html = html + "<td>"+(obj.data.harga)+"</td></tr>";
            html = html + "<tr><td>Jumlah</td>";
            html = html + "<td>"+(obj.data.jumlah)+"</td></tr>";
            html = html + "<tr><td>Jenis Paket</td>";
            html = html + "<td>"+(obj.data.paket)+"</td></tr>";
            html = html + "<tr><td>Nilai Barang</td>";
            html = html + "<td>Rp "+numberWithCommas(obj.data.nilai_brg)+"</td></tr>";
            html = html + "<tr><td colspan='2'><b>Detail Penerima</b></td>";
            html = html + "<tr><td>Atas Nama</td>";
            html = html + "<td>"+(obj.data.tr_nama)+"</td></tr>";
            html = html + "<tr><td>Telepon</td>";
            html = html + "<td>"+(obj.data.tr_telpon)+"</td></tr>";
            html = html + "<tr><td>Alamat</td>";
            html = html + "<td>"+(obj.data.tr_alamat)+"</td></tr>";
            html = html + "<tr><td colspan='2'><b>Status Pengiriman</b></td>";
            html = html + "<tr><td>Kantro TO Tujuan</td>";
            html = html + "<td>"+(obj.data.terima_to)+"</td></tr>";
            html = html + "<tr><td>Status</td>";
            html = html + "<td>"+status_kargo(obj.data.status_brg)+"</td></tr>";
            html = html + "<tr><td>Tanggal Terima</td>";
            html = html + "<td>"+tanggal_indo(obj.data.terima_tgl)+"</td></tr>";
            html = html + "<tr><td>Jam Terima</td>";
            html = html + "<td>"+(obj.data.terima_jam)+"</td></tr>";
            html = html + "<tr><td>Tanggal Ambil</td>";
            html = html + "<td>"+tanggal_indo(obj.data.ambil_tgl)+"</td></tr>";
            html = html + "<tr><td>Jam Ambil</td>";
            html = html + "<td>"+(obj.data.ambil_jam)+"</td></tr>";
            html = html + "<tr><td>Nama Pengambil</td>";
            html = html + "<td>"+(obj.data.ambil_nama)+"</td></tr>";
            html = html + "<tr><td>Telepon Pengambil</td>";
            html = html + "<td>"+(obj.data.ambil_nohp)+"</td></tr>";

            $(".kargo-result").html(html);
            $(".loading-animated").hide();
        } else {
            $(".loading-animated").hide();
            if (obj = data.responseJSON) {
                if (obj.status == '404') {
                    $("#error-alert").html("<p>Pengiriman dengan resi <b>"+$("#resi").val()+"</b> tidak ditemukan</p>");
                } else {
                    var error_message = "";
                    $.each(obj.data, function(key, value){
                        error_message = error_message + "<p>"+value+"</p>";
                    });
                    $("#error-alert").html("<p>"+error_message+"</p>");
                }
                $("#error-alert").show();
            } else {
                $("#error-alert").html("<p>Pastikan anda terhubungan dengan jaringan internet</p>");
                $("#error-alert").show();
            }
        }
    });
});
