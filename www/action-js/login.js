$("#login-action").click(function(){
    restPost(
        BASE_URL + 'api/login',
        {},
        {
            email       : $("#email").val(),
            password    : $("#password").val()
        },
        function (status, data) {
            obj = data.responseJSON;

            if (status == 'success') {
                console.log(obj);
                localStorage.setItem('ACC_ID', obj.data.id);
                localStorage.setItem('ACC_KEY', obj.data.key);
                html_success = "<p>" + obj.message + "</p>";
                $(".success-alert").html(html_success);
                $(".error-alert").hide();
                $(".success-alert").show();
                window.location = "pemesanan.html";
            } else {
                html_error = "<p>" + obj.message + "</p>";
                $(".error-alert").html(html_error);
                $(".error-alert").show();
                $(".success-alert").hide();
            }
        }
    );
});

$("#to-daftar-page-action").click(function(){
    window.location = "register.html";
});

$("#to-lupa-password").click(function(){
    window.location = "lupa-password.html";
});

$("#verification").click(function(){
    window.location = "register-verification.html";
});
