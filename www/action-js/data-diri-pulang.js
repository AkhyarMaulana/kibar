$(document).ready(function(){

    cart_obj = localStorage.getItem('CART_PULANG');
    checkout_obj = localStorage.getItem('CHECKOUT');
    
    /* request encript */
    enscripted_request = getUrlParameter('request');
    var request_obj = decriptJSON(enscripted_request);
    console.log("request_obj", request_obj);
    console.log("cart : ",cart_obj);
    console.log("checkout", checkout_obj);

    if (!cart_obj && !checkout_obj) {
        window.location = "riwayat-order.html";
    }

    localStorage.setItem('DATA_SHELTER_PULANG', "");
    localStorage.setItem('DATA_DROP_PULANG', "");

    var cart_obj = JSON.parse(cart_obj);
    var tanggal = cart_obj.tanggal;
    var jam = cart_obj.jam;
    var reg = cart_obj.reg;
    var kode = cart_obj.kd_produk;
    var cart_kursi = cart_obj.kursi;
    var jumlah_kursi = cart_kursi.length;

    restGET(BASE_URL + "api/userdetail",{
        "Acc-Id"  : localStorage.getItem('ACC_ID'),
        "Acc-Key" : localStorage.getItem('ACC_KEY')
    },function(status, data){
        obj = data.responseJSON;
        $("#title").val(obj.data.title);
        $("#fullname").val(obj.data.fullname);
        $("#alamat").val(obj.data.alamat);
        $("#phone").val(obj.data.phone);
    });

    restPost(BASE_URL + "api/ordercheck",{
        "Acc-Id"  : localStorage.getItem('ACC_ID'),
        "Acc-Key" : localStorage.getItem('ACC_KEY')
    },
    {
        tanggal : tanggal,
        reg : reg,
        kode : kode,
        kursi : cart_kursi,
        jam : jam,
        platform : "android",
    },function(status, data){
        obj = data.responseJSON;
        detail_order = obj.data.bus;

        if (status == 'success') {

            var berangkat = detail_order.berangkat;
            var tujuan = detail_order.tujuan;
            var bus = detail_order.kendaraan;

            FullDate = new Date(tanggal);
            var bulan = getBulan(tanggal);
            var tgl = getTanggal(tanggal);
            var tahun = getTahun(tanggal);
            var hari = getHariByIndex(FullDate.getDay());
            var berangkat_label = berangkat.substr(0,3);
            var tujuan_label = tujuan.substr(0,3);

            $(".small-bulan-tahun-hari").html(bulan + " " + tahun + '<br>' + hari);
            $(".big-tanggal").html(tgl);
            $(".destination-paragraph").html(berangkat_label + " - " + tujuan_label);
            $(".jam-berangkat").html(jam);
            $(".nama-mobil").html(bus);
            $(".penyelesaian-order").html("pukul " + obj.data.booking[0].akhir_blok);

            // $(".shelter-modal").html(generateShelterForModal(obj.data.shelter,jam));
            // $(".drop-modal").html(generateDropForModal(obj.data.drop,jam));
            restGET(BASE_URL + "api/listshelter?berangkat="+berangkat+"&tujuan="+tujuan, {

            }, function(status, data){
                obj = data.responseJSON;
                console.log(obj);
                if (status == 'success') {
                    $(".shelter-modal").html(generateShelterForModalPulang(obj.data,jam));
                }
            });

            restGET(BASE_URL + "api/listdrop?berangkat="+berangkat+"&tujuan="+tujuan, {

            }, function(status, data){
                obj = data.responseJSON;
                console.log(obj);
                if (status == 'success') {
                    $(".drop-modal").html(generateDropForModalPulang(obj.data,jam));
                }
            });

            if (jumlah_kursi > 1) {
                $(".header-kursi-label").html("Memesan");
                $(".header-kursi-nomor").html(cart_kursi.length + "<br> Kursi");
                $(".header-kursi-label").css("font-size", "12px");
                $(".header-kursi-label").css("padding-top", "8px");
            } else {
                $(".header-kursi-nomor").html(cart_kursi[0]);
            }
        } else {
            $(".modal-error-message").html(obj.message);
            $(".open-error-modal").click();
        }
    });

    $("#action-bayar").click(function(){

        var error = [];
        var shelter = localStorage.getItem('DATA_SHELTER_PULANG');
        var drop = localStorage.getItem('DATA_DROP_PULANG');
        var title = $("#title").val();
        var fullname = $("#fullname").val();
        var phone = $("#phone").val();
        var alamat = $("#alamat").val();

        var i = 0;

        if (!title) {
            error[i] = "Title harus diisi";
            i++;
        }

        if (!fullname) {
            error[i] = "Nama harus diisi";
            i++;
        }

        if (!phone) {
            error[i] = "Nomor telepon harus diisi";
            i++;
        }

        if (!alamat) {
            error[i] = "Alamat harus diisi";
            i++;
        }

        if (!shelter) {
            error[i] = "Shelter harus diisi";
            i++;
        }

        if (!drop) {
            error[i] = "Drop harus diisi";
            i++;
        }

        if (error.length > 0) {
            html_message = "";
            $.each(error, function(key, val){
                html_message = html_message + "<p>"+val+"</p>";
            });

            $("#param-error-message").html(html_message);
            $("#param-error-message").show();
        } else {

            restPost(BASE_URL + "api/userdetail",{
                "Acc-Id"  : localStorage.getItem('ACC_ID'),
                "Acc-Key" : localStorage.getItem('ACC_KEY')
            },{
                title : $("#title").val(),
                fullname : $("#fullname").val(),
                phone : $("#phone").val(),
                alamat : $("#alamat").val(),
            },function(status, data){
                obj = data.responseJSON;
                console.log(obj);
            });

            restPost(BASE_URL + "api/ordercheck",{
                "Acc-Id"  : localStorage.getItem('ACC_ID'),
                "Acc-Key" : localStorage.getItem('ACC_KEY')
            },
            {
                tanggal : tanggal,
                reg : reg,
                kode : kode,
                kursi : cart_kursi,
                jam : jam,
                platform : "android",
            },function(status, data){
                if (status == 'success') {
                    obj = data.responseJSON;
                    curent_cart = localStorage.getItem('CART');
                    localStorage.setItem('CHECKOUT', JSON.stringify({
                        cart : JSON.parse(curent_cart),
                        cart_pulang : cart_obj,
                        user : {
                            title : $("#title").val(),
                            fullname : $("#fullname").val(),
                            phone : $("#phone").val(),
                            alamat : $("#alamat").val(),
                            shelter : shelter,
                            drop : drop
                        }
                    }));

                    console.log(JSON.parse(localStorage.getItem('CHECKOUT')));
                    window.location = "pembayaran.html";
                } else {
                    $(".modal-error-message").html(obj.message);
                    $(".open-error-modal").click();
                }
            });
        }
    });

    height = $("body").height();
    $(".shelter-modal").height(parseInt(height) - 150);
    $(".drop-modal").height(parseInt(height) - 150);
});

$(document).on('hide.bs.modal','#error-modal', function () {
    window.history.back();
});

$(document).on('hide.bs.modal','#shelter', function () {
    if (localStorage.getItem("DATA_SHELTER_PULANG")) {
        $(".pilih-shelter-btn").html(localStorage.getItem("NAMA_SHELTER_PULANG"));
    } else {
        localStorage.removeItem("NAMA_SHELTER_PULANG")
    }
});

$(document).on('hide.bs.modal','#drop', function () {
    if (localStorage.getItem("DATA_DROP_PULANG")) {
        $(".pilih-drop-btn").html(localStorage.getItem("NAMA_DROP_PULANG"));
    } else {
        localStorage.removeItem("NAMA_DROP_PULANG")
    }
});
