$(document).ready(function(){
    function getAboutUs()
    {
        IMAGE_BASE_URL = BASE_URL.replace("api/", "");
        $.get(BASE_URL + "aboutus", function(data){
            obj = JSON && JSON.parse(data) || $.parseJSON(data);
            $("#about-text").html(obj.content);
        });
    }

    getAboutUs();
});
