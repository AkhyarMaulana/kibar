function show_kategori()
{
    $(".kategori-button").click();
    activity = "open kategori modal";
    console.log(activity);
}

function hide_kategori()
{
    $(".dimiss-modal-kategori").click();
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function toRupiah(str)
{
    str = String(str);
    str = str.replace(/(\d)(?=(\d{3})+$)/g, "$1.");
    return "Rp&nbsp;" + str;
}

function getNamaShelter(kode, pointer)
{
    restGET(BASE_URL + "api/shelter/" + kode,{
    }, function(status, data){
        obj = data.responseJSON;
        console.log(obj);
        $(pointer).html(obj.data.nama);
    });
}

function getNamaDrop(kode, pointer)
{
    restGET(BASE_URL + "api/drop/" + kode,{
    }, function(status, data){
        obj = data.responseJSON;
        console.log(obj);
        $(pointer).html(obj.data.nama);
    });
}

function waterMark(url)
{
    BASE_URL = BASE_URL.replace("/api/","/");
    url =  BASE_URL + "machine/watermark.php?photo="+url+"&stamp=" + BASE_URL + "asset/img/watermark.png";
    console.log("URL WATERMARK : " + url);
    return url;
}

function nl2br (str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

function DateConvert(str) {
    var date = new Date(str),
        mnth = ("0" + (date.getMonth()+1)).slice(-2),
        day  = ("0" + date.getDate()).slice(-2);
    return [ date.getFullYear(), mnth, day ].join("-");
}

function getBulan(str)
{
    var res = str.split("-");

    var bulan = [];
    bulan[1] = "Januari";
    bulan[2] = "Februari";
    bulan[3] = "Maret";
    bulan[4] = "April";
    bulan[5] = "Mei";
    bulan[6] = "Juni";
    bulan[7] = "Juli";
    bulan[8] = "Agustus";
    bulan[9] = "September";
    bulan[10] = "Oktober";
    bulan[11] = "November";
    bulan[12] = "December";

    return bulan[parseInt(res[1])];
}

function getTanggal(str)
{
    var res = str.split("-");
    return parseInt(res[2]);
}

function getTahun(str)
{
    var res = str.split("-");
    return parseInt(res[0]);
}

function getHari(str)
{
    day = str.getDay();

    var hari = [];
    hari[1] = "Senin";
    hari[2] = "Selasa";
    hari[3] = "Rabu";
    hari[4] = "Kamis";
    hari[5] = "Jum'at";
    hari[6] = "Sabtu";
    hari[7] = "Minggu";

    return hari[parseInt(day)];
}

function indonesiaFormatDatePicker()
{
    var i = 0;
    var hari = [];
    hari[1] = "Min";
    hari[2] = "Sen";
    hari[3] = "Sel";
    hari[4] = "Rab";
    hari[5] = "Kam";
    hari[6] = "Jum";
    hari[7] = "Sab";
    $(".c-datepicker__day-head").each(function(){
        i++;
        $(this).html(hari[i]);
    });
}

function terjemahBulan(Bulan)
{
    var Month = Bulan.split(" ")[0];
    var Year = Bulan.split(" ")[1];
    var List = {};
        List["January"] = "Januari";
        List["February"] = "Februari";
        List["March"] = "Maret";
        List["April"] = "April";
        List["May"] = "Mei";
        List["June"] = "Juni";
        List["July"] = "Juli";
        List["August"] = "Agustus";
        List["September"] = "September";
        List["October"] = "Oktober";
        List["November"] = "November";
        List["December"] = "Desember";
        
    var Bulan = List[Month];
    if (Bulan == undefined) {
        Bulan = Month;
    }
    return Bulan + " " + Year;
}

function terjemahHariIndonesia(curent_day)
{
    var day = {};
        day["Monday"] = "Senin"
        day["Tuesday"] = "Selasa"
        day["Wednesday"] = "Rabu"
        day["Thursday"] = "Kamis"
        day["Friday"] = "Jumat"
        day["Saturday"] = "Sabtu"
        day["Sunday"] = "Minggu"  
    return day[curent_day];
    
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

function today()
{
    return formatDate(new Date);
}

function getHariByIndex(index)
{
    var hari = [];
    hari[0] = "Minggu";
    hari[1] = "Senin";
    hari[2] = "Selasa";
    hari[3] = "Rabu";
    hari[4] = "Kamis";
    hari[5] = "Jum'at";
    hari[6] = "Sabtu";

    return hari[parseInt(index)];
}

function generateShelter(obj)
{
    console.log(obj);
    html = '';
    $.each(obj, function(key, val){
        html = html + '<option value="'+val.kode+'">'+val.nama+'</option>';
    });

    return html;
}

function zerotodoublezero(nilai)
{
    if (nilai == 0) {
        return "00";
    } else {
        return nilai;
    }
}

function tambahJam(jamfull, additional_menit)
{
    jam_array = jamfull.split(":");
    jam = parseInt(jam_array[0]);
    menit = parseInt(jam_array[1]);
    menit = menit + parseInt(additional_menit);
    mod_menit = menit % 60;
    int_menit = menit / 60;
    if (int_menit > 0) {
        jam = jam +  Math.round(int_menit);
    }

    if (jam > 24) {
        jam = jam - 24;
    }

    return jam + ':' + zerotodoublezero(mod_menit);
}

function minusJam(jamfull, additional_menit)
{
    jam_array = jamfull.split(":");
    jam = parseInt(jam_array[0]);
    menit = parseInt(jam_array[1]);

    total_menit = (jam * 60) + menit;    
    total_menit = total_menit - additional_menit;    

    jam_murni = Math.floor(total_menit / 60);    
    menit_murni = total_menit % 60;
    return jam_murni + ':' + zerotodoublezero(menit_murni);
}

function countSiap(plusminus, menit, jam)
{
    if (plusminus == "+") {
        return tambahJam(jam, menit);
    } else if (plusminus == "-") {
        return minusJam(jam, menit);
    } else {
        return jam;
    }
}

function generateShelterForModal(obj, jam, is_premium)
{
    console.log(obj);
    html = '';
    $.each(obj, function(key, val){
        if (is_premium && val.kode == 'SRONDOL') {
            return true;
        }
        siap = countSiap(val.plusminus, val.siap, jam);
        berangkat = countSiap(val.pm, val.berangkat, jam);
        html = html + '<div class="shelter-select-moda" data-shelter="'+val.kode+'">';
        html = html + '<p class="helter-paragraph">' +val.nama+'</p>';
        html = html + '<small>Siap : '+siap+' Berangkat : '+berangkat+'</small></div>';
    });

    html = html + `
        <script type="text/javascript">
            $(".shelter-select-moda").click(function(){
                data = $(this).attr("data-shelter");
                console.log("data shelter", data);
                nama_shelter = $(this).html();
                console.log("data shelter", nama_shelter);
                localStorage.setItem("DATA_SHELTER", data);
                localStorage.setItem("NAMA_SHELTER", nama_shelter);
                $(".modal-dimish").click();
            });
        </script>`;

    return html;
}

function generateDropForModal(obj, jam, is_premium)
{
    console.log(obj);
    html = '';
    $.each(obj, function(key, val){
        if (is_premium && val.kode == 'SRONDOL') {
            return true;
        }
        siap = countSiap(val.plusminus, val.siap, jam);
        berangkat = countSiap(val.pm, val.berangkat, jam);
        html = html + '<div class="drop-select-moda" data-drop="'+val.kode+'">';
        html = html + '<p class="drop-paragraph">' +val.nama+'</p></div>';
    });


    html = html + '<script type="text/javascript">$(".drop-select-moda").click(function(){data = $(this).attr("data-drop");nama_drop = $(this).html();localStorage.setItem("DATA_DROP", data);localStorage.setItem("NAMA_DROP", nama_drop);$(".modal-dimish").click();});<\/script>';

    return html;
}

function generateShelterForModalPulang(obj, jam)
{
    console.log(obj);
    html = '';
    $.each(obj, function(key, val){
        siap = countSiap(val.plusminus, val.siap, jam);
        berangkat = countSiap(val.pm, val.berangkat, jam);
        html = html + '<div class="shelter-select-moda" data-shelter="'+val.kode+'">';
        html = html + '<p class="helter-paragraph">' +val.nama+'</p>';
        html = html + '<small>Siap : '+siap+' Berangkat : '+berangkat+'</small></div>';
    });

    html = html + `
        <script type="text/javascript">
            $(".shelter-select-moda").click(function(){
                data = $(this).attr("data-shelter");
                console.log("data shelter", data);
                nama_shelter = $(this).html();
                console.log("data shelter", nama_shelter);
                localStorage.setItem("DATA_SHELTER_PULANG", data);
                localStorage.setItem("NAMA_SHELTER_PULANG", nama_shelter);
                $(".modal-dimish").click();
            });
        </script>`;

    return html;
}

function generateDropForModalPulang(obj, jam)
{
    console.log(obj);
    html = '';
    $.each(obj, function(key, val){
        siap = countSiap(val.plusminus, val.siap, jam);
        berangkat = countSiap(val.pm, val.berangkat, jam);
        html = html + '<div class="drop-select-moda" data-drop="'+val.kode+'">';
        html = html + '<p class="drop-paragraph">' +val.nama+'</p></div>';
    });


    html = html + '<script type="text/javascript">$(".drop-select-moda").click(function(){data = $(this).attr("data-drop");nama_drop = $(this).html();localStorage.setItem("DATA_DROP_PULANG", data);localStorage.setItem("NAMA_DROP_PULANG", nama_drop);$(".modal-dimish").click();});<\/script>';

    return html;
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function tanggal_indo(tanggal)
{
    if (tanggal != "" && tanggal != '0000-00-00') {
        return getTanggal(tanggal) + " " +
        getBulan(tanggal) + " " +
        getTahun(tanggal);
    } else {
        return "";
    }
}

function status_kargo(status)
{
    if (status == "K") {
        return "Proses Pengiriman";
    } else if (status == "T") {
        return "Telah diterima";
    } else if (status == "T") {
        return "Telah diambil";
    } else {
        return "Persiapan Pengiriman";
    }
}

function JenisPembayaranStringfy(jenis)
{
    if (jenis == 'mandiri') {
        return "Mandiri E - Channel";
    } else if (jenis == 'permata') {
        return "Permata Virtual Account"
    } else if (jenis == 'manual transfer') {
        return "Transfer Manual BCA"
    }
}

function encriptJSON(json_request_obj)
{
    var json_request_str = JSON.stringify(json_request_obj);
    var json_request_str_enc = window.btoa(json_request_str);
    return json_request_str_enc;
}

function decriptJSON(enscripted_request)
{
    var json_str_descripted = window.atob(enscripted_request);
    var request_obj = JSON.parse(json_str_descripted);
    return request_obj;
}

function jamStringfy(jam)
{
    if ($.isNumeric(jam)) {
        jam = parseInt(jam);
        if (jam < 10) {
            jam = "0" + jam + ":00";
        } else {
            jam = jam + ":00";
        }
        return jam;
    } else {
        return jam;
    }
}