function getInvoiceList(page)
{
    $(".loading-animated").show();
    if (!page) {
        page = 1;
    }
    restGET(BASE_URL + "api/listinvoice?page=" + page, {
        "Acc-Id"  : localStorage.getItem('ACC_ID'),
        "Acc-Key" : localStorage.getItem('ACC_KEY')
    }, function(status, data){
        if (status == 'success') {

            obj = data.responseJSON;
            console.log(obj);
            $.each(obj.data, function(key, value){
                html = '';
                tanggal = getTanggal(value.date_added) + " " +
                        getBulan(value.date_added) + " " +
                        getTahun(value.date_added);
                console.log(value);
                html = html + '<div class="travel-row invoice-row row" data-code="'+value.id+'">';
                html = html + '<div class="col-xs-3 travel-col">';
                html = html + '<img src="images/invoice-icon.png">';
                html = html + '</div>';
                html = html + '<div class="col-xs-6 travel-col">';
                html = html + '<p>Kode Booking <b>'+value.booking_code.toUpperCase()+'</b></p>';
                html = html + '<p>'+tanggal+'</p>';
                html = html + '<p><b>Rp '+numberWithCommas(value.harga_total)+'</b></p>';
                if (value.status_payment == 'waiting' && value.jenis_payment == 'manual transfer') {
                    html = html + '<p class="btn btn-xs btn-primary" style="font-size:11px;">Konfirmasi Sekarang</p>';
                }
                html = html + '</div>';
                html = html + '<div class="col-xs-3 travel-col">';
                html = html + '<p class="sisa-kursi-label">Status Payment</p>';
                html = html + '<p class="sisa-kursi-value" style="font-size:13px; font-weight:bold">'+capitalizeFirstLetter(value.status_payment)+'</p>';
                html = html + '</div>';
                html = html + '</div><div class="new-bus-action"></div>';
                html = html + '<script>';
                html = html + '$(".invoice-row").click(function(){';
                html = html + 'code = $(this).attr("data-code");';
                html = html + 'window.location = "invoice.html?id_invoice='+value.id+'";';
                html = html + 'console.log(decript);';
                html = html + '});<\/script>';
                $(".new-bus-action").replaceWith(html);
            });
            $(".loading-animated").hide();
        } else {
            if (page == 1) {
                html = "<div style='text-align:center'><h3>Invoice tidak ditemukan</h3>";
                $(".new-bus-action").replaceWith(html);
                $(".loading-animated").hide();
            } else {
                $(".stop-load").html("yes");
            }
        }
    });
}

var i = 1;

$(document).ready(function(){
    getInvoiceList(1);
});

$(window).lazyScrollLoading({
    onScrollToBottom : function(e, $lazyItems) {
        //$container.append($(html));
        i++;
        if ($(".stop-load").html() != "yes") {
            getInvoiceList(i);
        }
    }
});
