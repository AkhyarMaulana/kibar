$(document).ready(function(){
    restGET(BASE_URL + "api/userdetail", {
        "Acc-Id"  : localStorage.getItem('ACC_ID'),
        "Acc-Key" : localStorage.getItem('ACC_KEY')
    }, function(status, data){
        console.log(data.responseJSON);
        obj = data.responseJSON;
        $("#title").val(obj.data.title);
        $("#fullname").val(obj.data.fullname);
        $("#phone").val(obj.data.phone);
        $("#alamat").val(obj.data.alamat);
        $("#email").val(obj.data.email);
    });
});

$("#simpan-profil").click(function(){
    restPost(BASE_URL + "api/userdetail", {
        "Acc-Id"  : localStorage.getItem('ACC_ID'),
        "Acc-Key" : localStorage.getItem('ACC_KEY')
    },{
        title : $("#title").val(),
        fullname : $("#fullname").val(),
        phone : $("#phone").val(),
        alamat : $("#alamat").val()
    },function(status, data){
        console.log(data.responseJSON);
        if (status == 'success') {
            $(".pesan-sukses").html("<p>Berhasil update data</p>");
            $(".pesan-error").hide();
            $(".pesan-sukses").show();
        } else {
            $(".pesan-error").html("<p>Update gagal, tidak ada perubahan data</p>");
            $(".pesan-sukses").hide();
            $(".pesan-error").show();
        }
    });
});

$(".pesan-sukses").click(function(){
    $(".pesan-sukses").hide();
});
$(".pesan-error").click(function(){
    $(".pesan-error").hide();
});
