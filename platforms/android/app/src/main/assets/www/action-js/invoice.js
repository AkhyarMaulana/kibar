$(document).ready(function(){

    function getNamaShelter(kode)
    {
        restGET(BASE_URL + "api/shelter/" + kode,{
        }, function(status, data){
            obj = data.responseJSON;
            console.log(data);
            $(".shelter").html("Shelter " + obj.data.nama);
        });
    }

    function getNamaDrop(kode)
    {
        restGET(BASE_URL + "api/drop/" + kode,{
        }, function(status, data){
            obj = data.responseJSON;
            console.log(data);
            $(".drop").html("Drop " + obj.data.nama);
        });
    }

    var invoice_id = getUrlParameter('id_invoice');

    restGET(BASE_URL + "api/v2/getinvoice/" + invoice_id,{
        "Acc-Id"  : localStorage.getItem('ACC_ID'),
        "Acc-Key" : localStorage.getItem('ACC_KEY')
    }, function(status, data){
        obj = data.responseJSON;
        console.log(obj.data);
        tanggal = obj.data.tanggal;
        FullDate = new Date(tanggal);
        var bulan = getBulan(tanggal);
        var tgl = getTanggal(tanggal);
        var tahun = getTahun(tanggal);
        var hari = getHariByIndex(FullDate.getDay());

        $(".booking-code").html("Kode Booking <b>" + obj.data.booking_code.toUpperCase() + "</b>");
        $(".invoice-code").html(obj.data.invoice_code);
        $("#to-konfirmasi-pembayaran").attr("data-invoice-code", obj.data.invoice_code);
        $(".kendaraan").html(obj.data.kendaraan);
        $(".tanggal-big").html(tgl);
        $(".bulan-tahun").html(bulan + " " + tahun);
        $(".total-harga").html("Rp " + numberWithCommas(obj.data.harga_total));
        $(".perjalanan").html(obj.data.kode + " " + obj.data.jam);
        $(".fullname").html(obj.data.fullname);
        $(".alamat").html(obj.data.alamat);
        $(".phone").html(obj.data.phone);
        $(".total-payment").html("Rp " + numberWithCommas(obj.data.harga_total));
        jenis_payment = obj.data.jenis_payment;
        $(".jenis-pembayaran").html(JenisPembayaranStringfy(jenis_payment));
        $(".status-payment").html(obj.data.status_payment.toUpperCase());
        $("#virtual-account").html(obj.data.va);
        if (obj.data.atas_nama) {
            $("#atas-nama").html("<p>Atas Nama</p>" + "<b>" + obj.data.atas_nama + "</b>");
        }

        if (jenis_payment == 'manual transfer' && obj.data.status_payment == 'waiting') {
            $("#to-konfirmasi-pembayaran").show();
        }

        getNamaShelter(obj.data.shelter_code);
        getNamaDrop(obj.data.drop_code);
        $(".image-qr-code").html('<img src="'+BASE_URL+'api/invoiceqrcode?invoice_id='+obj.data.invoice_code+'">');
    

        /* get detail order */        
        for (let i = 0, p = Promise.resolve(); i < obj.data.item.length; i++) {
            p = p.then(_ => new Promise(function(resolve) {
                bus = obj.data.item[i]
                console.log("bus", bus);
                FullDate = new Date(bus.tanggal);
                bulan = getBulan(bus.tanggal);
                tgl = getTanggal(bus.tanggal);
                tahun = getTahun(bus.tanggal);
                hari = getHariByIndex(FullDate.getDay());
                html = `
                <div class="bus-list row">
                    <div class="col-xs-6">
                        <div class="date"><b>`+hari + " " + tgl + " " + bulan + " " + tahun+`</b>, <b>`+jamStringfy(bus.jam)+`</b></div>
                        <div class="vehicle-name">`+bus.kendaraan+`</div>
                        <div class="chair">Kursi `+(bus.kursi)+`</div>
                    </div>
                    <div class="col-xs-6">
                        <div class="product-code text-right">`+bus.kode+`</div>
                        <div class="reg-code text-right">`+bus.reg+`</div>
                        <div class="chair text-right">@Harga Rp<b>`+numberWithCommas(bus.harga_diskon)+`</b></div>
                    </div>
                </div>
                <div class="bus-list-new"></div>
                `;
                $(".bus-list-new").replaceWith(html);                
                resolve();
            }));
        }
    });


    $("#finish").click(function(){
        window.location = "riwayat-order.html";
    });
    $("#finished").click(function(){
        window.location = "pemesanan.html";
    });
    $("#to-konfirmasi-pembayaran").click(function(){
        var invoice_code = $("#to-konfirmasi-pembayaran").attr("data-invoice-code");
        window.location = "konfirmasi_pembayaran.html?invoice_code=" + invoice_code;
    });
});
