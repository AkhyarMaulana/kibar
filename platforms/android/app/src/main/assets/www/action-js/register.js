$("#to-login-page-action").click(function(){
    window.location = "login.html";
});

$("#register-action").click(function(){
    restPost(
        BASE_URL + 'api/register',
        {},
        {
            email               : $("#email").val(),
            password            : $("#password").val(),
            password_confirm    : $("#password_confirm").val(),
            title               : $("#title").val(),
            fullname            : $("#fullname").val(),
            phone               : $("#phone").val(),
        },
        function (status, data) {
            obj = data.responseJSON;

            if (status == 'success') {
                console.log(obj);
                html_success = "<p>" + obj.message + "</p>";
                $(".success-alert").html(html_success);
                $(".error-alert").hide();
                $(".success-alert").show();
                window.location = "register-verification.html";
            } else {
                console.log(obj);
                error_message = obj.data;
                html_error = "";
                $.each(error_message, function(key, val){
                    html_error = html_error + "<p>" + val + "</p>";
                });
                $(".error-alert").html(html_error);
                $(".error-alert").show();
                $(".success-alert").hide();
            }
        }
    );
});
