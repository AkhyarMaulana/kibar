$(document).ready(function(){
    berangkat = getUrlParameter('berangkat');
    tujuan = getUrlParameter('tujuan');
    tanggal = getUrlParameter('tanggal');    
    tanggal_pulang = getUrlParameter('tanggal_pulang');
    is_pulang_pergi = getUrlParameter('is_pulang_pergi');

    url = "berangkat=" + berangkat + "&";
    url = url + "tujuan=" + tujuan + "&";
    url = url + "tanggal=" + tanggal;

    enscripted_request = getUrlParameter('request');
    var request_obj = decriptJSON(enscripted_request);
    console.log("request_obj", request_obj);

    restGET(BASE_URL + "api/getbus?" + url, {

    }, function(status, data){
        if (status == 'success') {
            obj = data.responseJSON;
            console.log(obj);
            $.each(obj.data, function(key, value){
                html = '';
                console.log(value);
                bus_encript = berangkat + ':::' + tujuan + ':::';
                bus_encript = bus_encript + tanggal + ':::';
                bus_encript = bus_encript + value.jam + ':::';
                bus_encript = bus_encript + value.kendaraan + ':::';
                bus_encript = bus_encript + value.ke + ':::';
                html = html + '<div class="travel-row row" data-code="'+bus_encript+'" data-request="'+enscripted_request+'">';
                html = html + '<div class="col-xs-3 travel-col">';
                // html = html + '<img src="images/mobil-icon.png">';
                if (value.kursi <= 12) {
                    html = html + '<img src="images/emerald-icon.png" class="icon-bus">'; // hiace
                } else if (value.kursi > 12 && value.kursi < 21) {
                    html = html + '<img src="images/shuttle-icon.png" class="icon-bus">'; // mistubshi
                } else {
                    html = html + '<img src="images/bus-icon.png" class="icon-bus">';
                }
                html = html + '</div>';
                html = html + '<div class="col-xs-6 travel-col" style="padding:0px;">';
                html = html + '<p>'+value.jam+'</p>';
                html = html + '<p>'+value.kendaraan+' ('+value.kursi+')</p>';
                html = html + '<p><b>Rp '+value.tarip+'</b></p>';
                html = html + '</div>';
                html = html + '<div class="col-xs-3 travel-col">';
                if (value.status == 'extra') {
                    html = html + '<img src="images/extra-icon.png">';
                } else if (value.status == 'upgrade') {
                    html = html + '<img src="images/upgrade-icon.png">';
                } else {
                    html = html + '<img src="images/reguler-icon.png">';
                }
                html = html + '<p class="sisa-kursi-label">Sisa Kursi</p>';
                html = html + '<p class="sisa-kursi-value">'+value.sisa+'</p>';
                html = html + '</div>';
                html = html + '</div><div class="new-bus-action"></div>';
                html = html + '<script>';
                html = html + '$(".travel-row").click(function(){';
                html = html + 'code = $(this).attr("data-code");';
                html = html + 'request = $(this).attr("data-request");';
                // html = html + 'console.log("code : " , code);';
                html = html + 'window.location = "pilih_kursi_pulang.html?code=" + code + "&request=" + request;';
                html = html + '});<\/script>';
                $(".new-bus-action").replaceWith(html);
            });
            $(".loading-animated").hide();
        } else {
            html = "<div style='text-align:center'><h3>Bus tidak ditemukan</h3>";
            html = html + "<p>Silahkan berganti rute atau tanggal yang lain</p></div>";
            $(".new-bus-action").replaceWith(html);
            $(".loading-animated").hide();
        }
    });
});

$(document).ready(function(){
    berangkat = getUrlParameter('berangkat');
    tujuan = getUrlParameter('tujuan');
    tanggal = getUrlParameter('tanggal');

    berangkat_label = berangkat.substr(0,3);
    tujuan_label = tujuan.substr(0,3);

    html = '<p>'+berangkat_label+'<span class="to-circle">Ke</span>'+tujuan_label+'</p>';
    $(".destination-col").html(html);

    FullDate = new Date(tanggal);

    var bulan = getBulan(tanggal);
    var tgl = getTanggal(tanggal);
    var tahun = getTahun(tanggal);
    var hari = getHariByIndex(FullDate.getDay());

    $(".big-tanggal").html(tgl);
    $(".small-bulan-tahun-hari").html(bulan + " " + tahun + " <br> " + hari);
});
