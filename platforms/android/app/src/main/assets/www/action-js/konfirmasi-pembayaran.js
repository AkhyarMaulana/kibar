$(document).ready(function(){
    $(".loading-animated").show();
    $("#error-alert").hide();
    var invoice_code = getUrlParameter('invoice_code');

    restGET(BASE_URL + "api/konfirmasi/" + invoice_code,{
        "Acc-Id"  : localStorage.getItem('ACC_ID'),
        "Acc-Key" : localStorage.getItem('ACC_KEY')
    }, function(status, data){
        if (status == 'success') {
            obj = data.responseJSON;
            data = obj.data;

            console.log(data);
            tanggal = data.tanggal;

            FullDate = new Date(tanggal);
            var bulan = getBulan(tanggal);
            var tgl = getTanggal(tanggal);
            var tahun = getTahun(tanggal);
            var hari = getHariByIndex(FullDate.getDay());

            $(".bus-date").html("Keberangkatan <b>" + hari + ", " + tgl + " " + bulan +  " " + tahun  + "</b>");
            $(".invoice_code").html(data.invoice_code);
            $(".fullname").html(data.fullname);
            $("#invoice_code").val(data.invoice_code);

            $(".checkout-bus-tanggal").html(tgl + " " + bulan +  " " + tahun);
            $(".checkout-bus-jam").html(obj.data.jam);
            $(".checkout-bus-nama").html(obj.data.kendaraan);
            $(".checkout-bus-kode").html(obj.data.kode);
            $(".checkout-bus-reg").html(obj.data.reg);
            getNamaShelter(obj.data.shelter_code, '.checkout-bus-shelter');
            getNamaDrop(obj.data.drop_code, '.checkout-bus-drop');
            $(".checkout-bus-kursi").html(obj.data.kursi);

            $(".total").html("Rp " + numberWithCommas(obj.data.harga_total));


            $(".loading-animated").hide();
        } else {
            $(".loading-animated").hide();
            if (obj = data.responseJSON) {
                console.log(obj);
                if (obj.status == '404') {
                    $(".modal-error-message").html("<p>Invoice tidak ditemukan, atau sudah pernah dikonfirmasi</p>");
                    $(document).on('hide.bs.modal','#error-modal', function () {
                        window.history.back();
                    });
                } else {
                    var error_message = "";
                    $.each(obj.data, function(key, value){
                        error_message = error_message + "<p>"+value+"</p>";
                    });
                    $(".modal-error-message").html("<p>"+error_message+"</p>");
                }
                $(".open-error-modal").click();
            } else {
                $(".modal-error-message").html("<p>Pastikan anda terhubungan dengan jaringan internet</p>");
                $(".open-error-modal").click();
            }
        }
    });

    $("#konfirmasi-post").click(function(){
    	restPost(BASE_URL + "api/konfirmasi",{
            "Acc-Id"  : localStorage.getItem('ACC_ID'),
            "Acc-Key" : localStorage.getItem('ACC_KEY')
        }, {
            tanggal_bayar : $("#tanggal_bayar").val(),
            invoice_code : $("#invoice_code").val(),
            atas_nama : $("#atas_nama").val(),
            url_bukti_transfer : $("#url_bukti_transfer").val(),
            bank : $("#bank").val()
        },function(status, data){
            if (status == 'success') {
				obj = data.responseJSON;
	            console.log(obj.data);

                $(".modal-success-message").html("<p>Berhasil melakukan konfirmasi pembayaran</p>");
                $(document).on('hide.bs.modal','#success-modal', function () {
                    window.history.back();
                });
                $(".open-success-modal").click();
            } else {
				$(".loading-animated").hide();
				if (obj = data.responseJSON) {
                    console.log(obj);
                    if (obj.status == '404') {
                        $(".modal-error-message").html("<p>Invoice tidak ditemukan, atau sudah pernah dikonfirmasi</p>");
                        $(document).on('hide.bs.modal','#error-modal', function () {
                            window.history.back();
                        });
                    } else {
                        var error_message = "";
                        if (obj.data) {
                            $.each(obj.data, function(key, value){
                                error_message = error_message + "<p>"+value+"</p>";
                            });
                        } else {
                            error_message = obj.message;
                        }

                        $(".modal-error-message").html("<p>"+error_message+"</p>");
                    }
                    $(".open-error-modal").click();
                } else {
                    $(".modal-error-message").html("<p>Pastikan anda terhubungan dengan jaringan internet</p>");
                    $(".open-error-modal").click();
                }
			}
        });
    });
});
