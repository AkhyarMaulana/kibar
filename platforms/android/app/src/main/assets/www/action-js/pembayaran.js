$(document).ready(function(){
    cart_obj = localStorage.getItem('CART');
    checkout = localStorage.getItem('CHECKOUT');

    if (!cart_obj || !checkout) {
        window.location = "riwayat-order.html";
    }

    cart_pulang_obj = cart_pulang_str = localStorage.getItem('CART_PULANG');
    drop_code = localStorage.getItem('DATA_DROP');
    shelter_code = localStorage.getItem('DATA_SHELTER');
    drop_code_pulang = localStorage.getItem('DATA_DROP_PULANG');
    shelter_code_pulang = localStorage.getItem('DATA_SHELTER_PULANG');

    console.log("cart_obj", JSON.parse(cart_obj));
    console.log("checkout", JSON.parse(checkout));

    var cart_obj = JSON.parse(cart_obj);
    var checkout_obj = JSON.parse(checkout);

    var tanggal = cart_obj.tanggal;
    var jam = cart_obj.jam;
    var reg = cart_obj.reg;
    var kode = cart_obj.kd_produk;
    var cart_kursi = cart_obj.kursi;
    var jumlah_kursi = cart_kursi.length;

    listItem(tanggal, reg, kode, cart_kursi, jam, shelter_code, drop_code);
    if (cart_pulang_obj != "" && cart_pulang_obj != undefined) {
        var cart_pulang_obj = JSON.parse(cart_pulang_obj);
        var tanggal_pulang = cart_pulang_obj.tanggal;
        var jam_pulang = cart_pulang_obj.jam;
        var reg_pulang = cart_pulang_obj.reg;
        var kode_pulang = cart_pulang_obj.kd_produk;
        var cart_kursi_pulang = cart_pulang_obj.kursi;
        var jumlah_kursi_pulang = cart_kursi_pulang.length;
        listItem(tanggal_pulang, reg_pulang, kode_pulang, cart_kursi_pulang, jam_pulang, shelter_code_pulang, drop_code_pulang);

        $(document).on('hide.bs.modal','#error-modal', function () {
            window.history.go(-6);
        });
    } else {
        $(document).on('hide.bs.modal','#error-modal', function () {
            window.history.go(-2);
        });
    }

    function listItem(tanggal, reg, kode, cart_kursi, jam, shelter_code, drop_code)
    {
        restPost(BASE_URL + "api/ordercheck",{
            "Acc-Id"  : localStorage.getItem('ACC_ID'),
            "Acc-Key" : localStorage.getItem('ACC_KEY')
        },
        {
            tanggal : tanggal,
            reg : reg,
            kode : kode,
            kursi : cart_kursi,
            jam : jam,
            platform : "android",
        },function(status, data){
            obj = data.responseJSON;
            console.log(obj);

            if (status == 'success') {
                FullDate = new Date(tanggal);
                var bulan = getBulan(tanggal);
                var tgl = getTanggal(tanggal);
                var tahun = getTahun(tanggal);
                var hari = getHariByIndex(FullDate.getDay());
                kursi = cart_obj.kursi;

                $(".bus-berangkat").html(obj.data.bus.berangkat);
                $(".bus-tujuan").html(obj.data.bus.tujuan);
                $(".bus-date").html(hari + " " + tgl + " " + bulan + " " + tahun);
                $(".checkout-fullname").html(capitalizeFirstLetter(checkout_obj.user.title)
                 + ". " + checkout_obj.user.fullname);
                $(".checkout-phone").html(checkout_obj.user.phone);
                $(".checkout-alamat").html(checkout_obj.user.alamat);                

                kursi.forEach(function(chair){
                    html = `
                    <div class="bus-list row">
                        <div class="col-xs-6">
                            <div class="date"><b>`+hari + " " + tgl + " " + bulan + " " + tahun+`</b>, <b>`+jamStringfy(jam)+`</b></div>
                            <div class="vehicle-name">`+obj.data.bus.kendaraan+`</div>
                            <div class="product-code">`+kode+`</div>
                            <div class="reg-code">`+reg+`</div>
                            <div class="chair">Kursi `+chair+`</div>
                        </div>
                        <div class="col-xs-6">
                            <div class="depart-city text-right"><b>`+obj.data.bus.berangkat+`</b></div>
                            <div class="depart-shelter text-right" shelter-data="`+shelter_code+`"></div>
                            <div class="destination-city text-right"><b>`+obj.data.bus.tujuan+`</b></div>
                            <div class="destination-drop text-right" drop-data="`+drop_code+`"></div>
                        </div>
                    </div>
                    <div class="bus-list-new"></div>
                    `;
                    $(".bus-list-new").replaceWith(html);
                });
            } else {
                if ($(".modal-error-message").html() == "") {
                    $(".modal-error-message").html(obj.message);
                    $(".open-error-modal").click();
                }
            }
        })
    }

    var obj = checkout_obj;

    $(".fullname").html(capitalizeFirstLetter(obj.user.title) + ". " + obj.user.fullname);
    $(".phone").html(obj.user.phone);

    $("#bayar-action").click(function(){
        $(".loading-blok").show();
        
        jenis_pembayaran = $("#jenis-payment").val();
        var checkout_param = {};
        checkout_param["userdata"] = {
            platform : "android",
            title : capitalizeFirstLetter(checkout_obj.user.title),
            fullname : checkout_obj.user.fullname,
            phone : checkout_obj.user.phone,
            alamat : checkout_obj.user.alamat,
            jenis_payment : jenis_pembayaran
        };
        checkout_param["berangkat"] = {
            tanggal : tanggal,
            reg : reg,
            kode : kode,
            kursi : cart_kursi,
            jam : jam,
            shelter_code : shelter_code,
            drop_code : drop_code
        };
        if (cart_pulang_str != undefined && cart_pulang_str != "") {
            checkout_param["pulang"] = {
                tanggal : tanggal_pulang,
                reg : reg_pulang,
                kode : kode_pulang,
                kursi : cart_kursi_pulang,
                jam : jam_pulang,
                shelter_code : shelter_code_pulang,
                drop_code : drop_code_pulang
            };
        }

        restPost(
            BASE_URL + 'api/v2/checkout',
            {
                "Acc-Id"  : localStorage.getItem('ACC_ID'),
                "Acc-Key" : localStorage.getItem('ACC_KEY')
            }, checkout_param,
            function (status, data) {
                objek = data.responseJSON;
                console.log(objek);
                if (status == 'success') {
                    localStorage.removeItem('CART');
                    localStorage.removeItem('CHECKOUT');
                    window.location = "invoice.html?id_invoice=" + objek.data.invoice.id_invoice;
                } else {
                    $(".modal-error-message").html("<p>Kursi telah terlebih dahulu dipesan oleh orang lain, Silahkan pilih kembali kursi yang diinginkan</p>");
                    $(".open-error-modal").click();
                }
            }
        );
    });
});

$("#pay-va-bca").click(function(){
    $(".select-active").hide();
    $(".select-nonactive").show();
    $("#select-nonactive-bca-va").hide();
    $("#select-active-bca-va").show();
    $("#jenis-payment").val("bca");
});

$("#pay-va-bni").click(function(){
    $(".select-active").hide();
    $(".select-nonactive").show();
    $("#select-nonactive-bni-va").hide();
    $("#select-active-bni-va").show();
    $("#jenis-payment").val("bni");
});

$("#pay-mandiri-e-channel").click(function(){
    $(".select-active").hide();
    $(".select-nonactive").show();
    $("#select-nonactive-mandiri-e-channel").hide();
    $("#select-active-mandiri-e-channel").show();
    $("#jenis-payment").val("mandiri");
});

$("#pay-va-permata").click(function(){
    $(".select-active").hide();
    $(".select-nonactive").show();
    $("#select-nonactive-permata-va").hide();
    $("#select-active-permata-va").show();
    $("#jenis-payment").val("permata");
});

$("#pay-saldo").click(function(){
    $(".select-active").hide();
    $(".select-nonactive").show();
    $("#select-nonactive-saldo").hide();
    $("#select-active-saldo").show();
    $("#jenis-payment").val("SALDO");
});

$("#pay-manual-transfer").click(function(){
    $(".select-active").hide();
    $(".select-nonactive").show();
    $("#select-nonactive-manual-transfer").hide();
    $("#select-active-manual-transfer").show();
    $("#jenis-payment").val("manual transfer");
});

$(".loading-blok").height($(window).height());
