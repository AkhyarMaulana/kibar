$(document).ready(function(){
    /* Clean user local data cache*/
    localStorage.removeItem('CART');
    localStorage.removeItem('CART_PULANG');
    localStorage.removeItem('CHECKOUT');
    localStorage.removeItem('DATA_DROP');
    localStorage.removeItem('DATA_DROP_PULANG');
    localStorage.removeItem('DATA_SHELTER');
    localStorage.removeItem('DATA_SHELTER_PULANG');


    /* initialization variable */
    code = getUrlParameter('code');
    explode = code.split(":::");
    berangkat = explode[0];
    tujuan = explode[1];
    tanggal = explode[2];
    jam = explode[3];
    bus = explode[4];
    reg = explode[5];

    /* request encript */
    enscripted_request = getUrlParameter('request');
    var request_obj = decriptJSON(enscripted_request);
    console.log("request_obj", request_obj);

    FullDate = new Date(tanggal);

    var bulan = getBulan(tanggal);
    var tgl = getTanggal(tanggal);
    var tahun = getTahun(tanggal);
    var hari = getHariByIndex(FullDate.getDay());
    var berangkat_label = berangkat.substr(0,3);
    var tujuan_label = tujuan.substr(0,3);

    $(".small-bulan-tahun-hari").html(bulan + " " + tahun + '<br>' + hari);
    $(".big-tanggal").html(tgl);
    $(".destination-paragraph").html(berangkat_label + " - " + tujuan_label);
    $(".jam-berangkat").html(jam);
    $(".nama-mobil").html(bus);

    restGET(BASE_URL + "api/getcair?berangkat="+berangkat+"&tujuan="+tujuan+"&tanggal="+tanggal+"&jam="+jam+"&kendaraan=" + bus + "&reg=" + reg,
    {
        "Acc-Id"  : localStorage.getItem('ACC_ID'),
        "Acc-Key" : localStorage.getItem('ACC_KEY')
    }, function(status, data){
        if (status == 'success') {
            obj = data.responseJSON;
            console.log(obj);
            cairs = obj.data.detail_kursi;
            $.each(cairs, function(key, val){
                text_jungling = "";
                if (key < 10) {
                    text_jungling = "0";
                }
                if (val == 'Order') {
                    $(".kursi-" + text_jungling + key).addClass("locked");
                }

                if (val == 'Paid') {
                    $(".kursi-" + text_jungling + key).addClass("sold");
                }

                if (val == 'Locked') {
                    $(".kursi-" + text_jungling + key).addClass("booked");
                }
            });
            $("#kode-bus").val(obj.data.kode);
            if (obj.data.kursi == 12) {
                $(".skema-12").show();
                $(".skema-13").remove();
                $(".skema-21").remove();
                $(".skema-24").remove();
                $(".skema-7").remove();
                $(".loading-animated").hide();
                $(".button-container").show();
            } else if (obj.data.kursi == 13) {
                $(".skema-12").remove();
                $(".skema-13").show();
                $(".skema-21").remove();
                $(".skema-24").remove();
                $(".skema-7").remove();
                $(".loading-animated").hide();
                $(".button-container").show();
            } else if (obj.data.kursi == 21) {
                $(".skema-12").remove();
                $(".skema-13").remove();
                $(".skema-21").show();
                $(".skema-24").remove();
                $(".skema-7").remove();
                $(".loading-animated").hide();
                $(".button-container").show();
            } else if (obj.data.kursi == 24) {
                $(".skema-12").remove();
                $(".skema-13").remove();
                $(".skema-21").remove();
                $(".skema-7").remove();
                $(".skema-24").show();
                $(".loading-animated").hide();
                $(".button-container").show();
            } else if (obj.data.kursi == 7) {
                $(".skema-12").remove();
                $(".skema-13").remove();
                $(".skema-21").remove();
                $(".skema-24").remove();
                $(".skema-7").show();
                $(".loading-animated").hide();
                $(".button-container").show();
            }
        } else {
            obj = data.responseJSON;
            if (obj) {
                var message = obj.message;
            } else {
                var message = "Bus tidak ditemukan";
            }

            html = "<div style='text-align:center'><h3>"+message+"</h3>";
            html = html + "<p>Silahkan berganti rute atau tanggal yang lain</p></div>";
            $(".body-container").html(html);
            $(".loading-animated").hide();
        }
    });

    $(".kursi").click(function(){
        var kursi = $(this).attr("data-kursi");
        if ($(".kursi-" + kursi).hasClass("checked")) {
            $(".kursi-" + kursi).removeClass("checked");
        } else {
            restPost(
                BASE_URL + 'api/checkcair',
                {},
                {
                	tanggal : tanggal,
                	reg : reg,
                	kd_produk : $("#kode-bus").val(),
                	kursi : kursi,
                	jam : jam,
                	platform : "android"
                },
                function (status, data) {
                    obj = data.responseJSON;
                    console.log(obj);
                    if (status == 'success') {
                        $(".kursi-" + kursi).addClass("checked");
                    } else {
                        error_message = obj.data;
                        html_error = "";
                        $.each(error_message, function(key, val){
                            html_error = html_error + "<p>" + val + "</p>";
                        });
                        $(".modal-error-message").html(html_error);
                        $(".open-error-modal").click();
                    }
                }
            );
        }
    });

    $("#pengisian-idenfitas").click(function(){
        var kursi_terpilih = [];
        var i = 0;
        $(".kursi").each(function(){
            is_checked = $(this).hasClass("checked");
            if (is_checked) {
                kursi = $(this).attr("data-kursi");
                kursi_terpilih[i] = kursi;
                i++;
            }
        });

        if (kursi_terpilih.length < 1) {

            $(".modal-error-message").html("<p>Anda belum memilih kursi</p>");
            $(".open-error-modal").click();

        } else {

            localStorage.setItem('CART', JSON.stringify({
                tanggal : tanggal,
                reg : reg,
                kd_produk : $("#kode-bus").val(),
                kursi : kursi_terpilih,
                jam : jam,
                platform : "android"
            }));

            cart_obj = localStorage.getItem('CART');
            console.log(JSON.parse(cart_obj));

            restPost(
                BASE_URL + 'api/lockcair',
                {
                    "Acc-Id"  : localStorage.getItem('ACC_ID'),
                    "Acc-Key" : localStorage.getItem('ACC_KEY')
                },
                {
                	tanggal : tanggal,
                	reg : reg,
                	kd_produk : $("#kode-bus").val(),
                	kursi : kursi_terpilih,
                	jam : jam,
                	platform : "android",
                },
                function (status, data) {
                    obj = data.responseJSON;
                    console.log(obj);
                    if (status == 'success') {
                        window.location = "data_diri.html?request=" + enscripted_request;
                    } else {
                        if (obj.status == '401') {
                            login_link = '<a onclick="window.location = \'login.html\';" class="btn btn-primary" style="margin:0 auto;">Login</a>';
                            $(".modal-error-message").html("<p>Sesi login telah habis</p>" + login_link);
                            $(".open-error-modal").click();
                            $(".btn-danger").hide();

                        } else {
                            $(".modal-error-message").html("<p>Kursi tidak lagi tersedia, Silahkan refresh untuk memperbarui list kursi</p>");
                            $(".open-error-modal").click();
                        }
                    }
                }
            );
        }
    });

    $(".refresh-btn").click(function(){
        location.reload();
    });
});
