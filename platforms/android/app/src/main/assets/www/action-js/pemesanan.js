$(document).ready(function(){
    restGET(BASE_URL + "api/getcity", {

    }, function(status, data){
        obj = data.responseJSON;
        select_html = "";
        $.each(obj.data, function(key, value){
            select_html = select_html + "<option value='" + value.kota + "'>" + value.kota + "</option>";
        });
        select_keberangkatan = "<option value=''> - Pilih Kota Keberangkatan - </option>" + select_html;
        $("#select-keberangkatan").html(select_keberangkatan);
        select_tujuan = "<option value=''> - Pilih Kota Tujuan - </option>" + select_html;
        $("#select-tujuan").html(select_tujuan);
    });
});

$(document).ready(function(){
    today = FullDate = formatDate(new Date);
    $("#tanggal").val(today);
    var bulan = getBulan(FullDate);
    var tanggal = getTanggal(FullDate);
    var tahun = getTahun(FullDate);
    d = new Date;
    var hari = getHariByIndex(d.getDay());

    $(".date-pic-date").html(tanggal);
    $(".date-pic-month-years").html(bulan + " " + tahun + '<br>' + hari);
});

function onPulangViewRender()
{
    /* tanggal pulang */
    var FullDate = formatDate(new Date);
    var today = FullDate;
    console.log("Fulldate pulang", FullDate);
    $("#tanggal_pulang").val(today);
    var bulan = getBulan(FullDate);
    var tanggal = getTanggal(FullDate);
    var tahun = getTahun(FullDate);
    d = new Date;
    var hari = getHariByIndex(d.getDay());
    console.log("tanggal pulang", tanggal);
    $(".date-pic-date-pulang").html(tanggal);
    $(".date-pic-month-years-pulang").html(bulan + " " + tahun + '<br>' + hari);
};


$("#action-cari-keberangkatan").click(function(){
    berangkat = $("#select-keberangkatan").val();
    tujuan = $("#select-tujuan").val();
    tanggal = $("#tanggal").val();
    tanggal_pulang = $("#tanggal_pulang").val();
    if ($("#pulang_pergi_check").prop('checked')) {
        is_pulang_pergi = 'yes';
    } else {
        is_pulang_pergi = 'no';
    }
    
    var d1 = new Date(tanggal);
    var d2 = new Date();
    error = "";
    if (berangkat == "") {
        error = error + "<p>Silankan pilih kota keberangkatan</p>";
    }
    if (tujuan == "") {
        error = error + "<p>Silankan pilih kota tujuan</p>";
    }
    if (tanggal == "") {
        error = error + "<p>Silankan pilih tanggal keberangkatan</p>";
    } else {
        if (formatDate(d1) == formatDate(d2)) {
            console.log("same date, it's ok");
        } else {
            if (d1 < d2) {
                error = error + "<p>Tidak dapat memilih tanggal yang telah lalu</p>";
            }
        }
    }
    if (is_pulang_pergi == 'yes') {
        var db = new Date(tanggal);
        var dp = new Date(tanggal_pulang);
        if (tanggal_pulang == "") {
            error = error + "<p>Silankan pilih tanggal pulang</p>";
        } else {
            if (formatDate(db) == formatDate(dp)) {
                console.log("same date, it's ok");
            } else {
                if (db > dp) {
                    error = error + "<p>Tidak dapat memilih tanggal pulang sebelum tanggal keberangkatan</p>";
                }
            }
        }
    }
    if (error) {
        $(".error-alert").html(error)
        $(".error-alert").show();
        window.scrollTo(0, 0);
    } else {
        url = "berangkat=" + berangkat + "&";
        url = url + "tujuan=" + tujuan + "&";
        url = url + "tanggal=" + tanggal + "&";
        url = url + "tanggal_pulang=" + tanggal_pulang + "&";
        url = url + "is_pulang_pergi=" + is_pulang_pergi;
        console.log(url);
        window.location = "bus_list.html?" + url;
    }
});

$("#pulang-pergi").click(function(){
    $(".tanggal-pulang-form").fadeToggle();
    if ($("#pulang_pergi_check").prop('checked')) {
        console.log("checked");
        $("#pulang_pergi_check").prop("checked", false);
    } else {
        $("#pulang_pergi_check").prop("checked", true);
        console.log("not checked");
        onPulangViewRender();
    }
});

$(document).ready(function(){
    /* pulang pergi deactive by default */
    $("#pulang_pergi_check").prop("checked", false);
});